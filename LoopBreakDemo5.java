import java.util.Scanner;

public class LoopBreakDemo5 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the row");
		int row = scan.nextInt();
		for (int i = 1; i <= row; i++) {
			for (int j = 1; j < row - i + 1; j++) {
				System.out.print("0 ");

			}

			System.out.println();
		}
	}
}

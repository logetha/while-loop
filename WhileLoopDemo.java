
public class WhileLoopDemo {
	public static void main(String[] args) {
		
		int a = 1;
		int b = 1;
		int c = 7;
		int d = 1;
		while (b<=7) {
			while (a<=7) {
				if (a>=c) {
					System.out.print(d);
				}else {
					System.out.print("1");
				}
				a++;
			}
			System.out.print("\n");
			b++;
			a=1;
			c--;
			d++;
		}
	}
}